package com.etisalat.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.etisalat.entities.DepartmentEntity;

@Repository
public interface DepartmentRepository  extends JpaRepository<DepartmentEntity, Integer>{

		
      @Query(value = "SELECT * FROM departments",nativeQuery =true)
      
      List<DepartmentEntity> getAllDepartmentsNames();

}
