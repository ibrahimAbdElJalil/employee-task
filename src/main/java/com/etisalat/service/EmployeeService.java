package com.etisalat.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.etisalat.dao.DepartmentRepository;
import com.etisalat.dao.EmployeeRepository;
import com.etisalat.entities.DepartmentEntity;
import com.etisalat.entities.EmployeeEntity;

@Service
public class EmployeeService implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	DepartmentRepository departmentRepository;
	
	public EmployeeEntity createOrUpdateEmployee(EmployeeEntity employee) {
		
		Optional<EmployeeEntity> emp  = null;
		Optional<DepartmentEntity> dep  = null;
		if(employee.getEmployeeID()!=null)
			emp = employeeRepository.findById(employee.getEmployeeID());
		
		if(employee.getDepartment()!= null && employee.getDepartment().getDepartmentID() != null)
			dep = departmentRepository.findById(employee.getDepartment().getDepartmentID());
            
        if(employee.getEmployeeID()!=null && emp.isPresent() ) { 
        	
            EmployeeEntity newEntityToBeSaved = emp.get();
            newEntityToBeSaved.setFirstName(employee.getFirstName());
            newEntityToBeSaved.setLastName(employee.getLastName());
            newEntityToBeSaved.setEmail(employee.getEmail());
            newEntityToBeSaved.setHireDate(employee.getHireDate());
            newEntityToBeSaved.setPhoneNumber(employee.getPhoneNumber());
            newEntityToBeSaved.setSalary(employee.getSalary());
            if(employee.getManager() != null && employee.getManager().getEmployeeID() != null) {
            newEntityToBeSaved.setManager(employeeRepository.findById(employee.getManager().getEmployeeID()).get());
            }else {
            	newEntityToBeSaved.setManager(null);
            }if(employee.getDepartment()!= null && employee.getDepartment().getDepartmentID() != null) {
            newEntityToBeSaved.setDepartment(departmentRepository.findById(employee.getDepartment().getDepartmentID()).get());
            }
            else {
            	newEntityToBeSaved.setDepartment(null);
            }
         return   employeeRepository.save(newEntityToBeSaved);
        }else {
        	 if(employee.getManager()!=null && employee.getManager().getEmployeeID()!=null&&!"".equals(employee.getManager().getEmployeeID())) {// && 
        		 employee.setManager(employeeRepository.findById(employee.getManager().getEmployeeID()).get());
        	 }else{
        		 employee.setManager(null);
        	 }if(employee.getDepartment()!=null && employee.getDepartment().getDepartmentID()!= null&&!"".equals(employee.getDepartment().getDepartmentID())) {
        		 employee.setDepartment(departmentRepository.findById(employee.getDepartment().getDepartmentID()).get());
        	 }else {
        		 employee.setDepartment(null);
        	 }
        	 employeeRepository.save(employee);
        }
		return employee;	
	}

	public  List<EmployeeEntity> readEmployees()  {
		 List<EmployeeEntity> employees =  employeeRepository.findAll();
		  
		 if(employees.size() >0) {
			 return employees;
		 }else {
	            return new ArrayList<EmployeeEntity>();
		 }
	}
	
	public EmployeeEntity readOneEmployees(Integer empID) throws Exception {
        Optional<EmployeeEntity> empEntity = employeeRepository.findById(empID);
        
        if(empEntity.isPresent()) {
        	return empEntity.get();
        }else {
            throw new Exception("No employee  exist");
        }
	}

	public HttpStatus deleteEmployee(Integer empID) throws Exception {
        Optional<EmployeeEntity> empEntity = employeeRepository.findById(empID);
              if(empEntity.isPresent()) {
          		employeeRepository.deleteById(empID);

              }else {
                  throw new Exception("Employee not found to delete");
              }
			return HttpStatus.OK;
	}

     public List<DepartmentEntity> getAllDepartmentsByName()  {
    	List<DepartmentEntity> departments =  departmentRepository.getAllDepartmentsNames();
		return departments;
	}

}
