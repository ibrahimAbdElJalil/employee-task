package com.etisalat.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.etisalat.dao.DepartmentRepository;
import com.etisalat.dao.EmployeeRepository;
import com.etisalat.entities.DepartmentEntity;
import com.etisalat.entities.EmployeeEntity;

@Service
public class DepartmentService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	DepartmentRepository departmentRepository;

	@Autowired
	EmployeeRepository employeeRepository;

	public DepartmentEntity createOrUpdateDepartment(DepartmentEntity department) {

		Optional<DepartmentEntity> dep = null;
		if(department.getDepartmentID() != null)
	        dep = departmentRepository.findById(department.getDepartmentID());

		if (department.getDepartmentID()!= null && dep.get() != null) {

			DepartmentEntity newEntityToBeSaved = dep.get();
			if (department.getManager().getEmployeeID() != null) {
				newEntityToBeSaved.setManager(employeeRepository.findById(department.getManager().getEmployeeID()).get());
			}
			newEntityToBeSaved.setDepartmentName(department.getDepartmentName());

			return newEntityToBeSaved;
		} else {
			if(department.getManager().getEmployeeID() != null ) {
				departmentRepository.save(department);
			}else {
				department.setManager(null);
				departmentRepository.save(department);
			}
		}
		return department;

	}

	public List<DepartmentEntity> readDepartments() {
		List<DepartmentEntity> departments = departmentRepository.findAll();

		if (departments.size() > 0) {
			return departments;
		} else {
			return new ArrayList<DepartmentEntity>();
		}
	}

	public DepartmentEntity readOneDepartments(Integer deptID) throws Exception {
		Optional<DepartmentEntity> deptEntity = departmentRepository.findById(deptID);

		if (deptEntity.isPresent()) {
			return deptEntity.get();
		} else {
			throw new Exception("No department  exist");
		}
	}

	public HttpStatus deleteDepartment(Integer deptID) throws Exception {
		Optional<DepartmentEntity> empEntity = departmentRepository.findById(deptID);
		if (empEntity.isPresent()) {
			departmentRepository.deleteById(deptID);

		} else {
			throw new Exception("Department not found to delete");
		}
		return HttpStatus.OK;
	}

}
