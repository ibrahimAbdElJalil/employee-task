package com.etisalat.entities;

import java.io.Serializable;
import java.util.Date;
import com.etisalat.utils.*;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "employees")
public class EmployeeEntity implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "employee_id")
	private Integer employeeID;

	@Column(name = "first_name", nullable = false)
	@Length(min = 2)
	private String firstName;

	@Column(name = "last_name", nullable = false)
	@Length(min = 2)
	private String lastName;

	@Column(name = "email")
	private String email;

	@Column(name = "phone_number")
	private Integer phoneNumber;

	@Column(name = "hire_date")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date hireDate;

	@Column(name = "salary")
	private Double salary;

	public EmployeeEntity() {
		super();
	}

	public EmployeeEntity(Integer employeeID) {
		super();
		this.employeeID = employeeID;
	}

	@ManyToOne(optional = true,cascade = {CascadeType.ALL})
	@JoinColumn(name = "department_id",referencedColumnName = "department_id")
	@JsonView(View.Summary.class)
	private DepartmentEntity department;

	@ManyToOne(optional = true,cascade = {CascadeType.DETACH})
	@JoinColumn(name = "managerID", referencedColumnName = "employee_id")
	@JsonView(View.Summary.class)
	private EmployeeEntity manager;

	@JsonProperty("employeeID")
	public Integer getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Integer employeeID) {
		this.employeeID = employeeID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public EmployeeEntity getManager() {
		return manager;
	}

	public void setManager(EmployeeEntity manager) {
		this.manager = manager;
	}

	public DepartmentEntity getDepartment() {
		return department;
	}

	public void setDepartment(DepartmentEntity department) {
		this.department = department;
	}

}
