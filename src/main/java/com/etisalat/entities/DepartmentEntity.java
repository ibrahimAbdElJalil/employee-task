package com.etisalat.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.etisalat.utils.View;
import com.fasterxml.jackson.annotation.JsonView;



@Entity
@Table(name="departments")
public class DepartmentEntity {
	
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 @Column(name = "department_id")
	 private Integer departmentID;
	 
	 @Column(name = "department_name")
	 private String departmentName;
	 
	 
	 @OneToOne(cascade = {CascadeType.DETACH})
	 @JoinColumn(name="managerID",referencedColumnName="employee_id")
	 @JsonView(View.Summary.class)
	 private EmployeeEntity manager;

	public Integer getDepartmentID() {
		return departmentID;
	}

	public void setDepartmentID(Integer departmentID) {
		this.departmentID = departmentID;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public EmployeeEntity getManager() {
		return manager;
	}

	public void setManager(EmployeeEntity manager) {
		this.manager = manager;
	}



	 
	 

}
