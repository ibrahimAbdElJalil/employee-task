package com.etisalat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.etisalat.entities.DepartmentEntity;
import com.etisalat.entities.EmployeeEntity;
import com.etisalat.service.EmployeeService;

@Controller
@CrossOrigin(origins = {"http://localhost:8081","http://localhost:3000"})
@RequestMapping("/employee")
public class EmployeeContoller {
	
	@Autowired
	EmployeeService employeeService;
	
	
	@PostMapping("/createOrUpdate")
	public ResponseEntity<?> createEmployee (@RequestBody EmployeeEntity employee) {
		
		EmployeeEntity empEntity = employeeService.createOrUpdateEmployee(employee);
		
		return  ResponseEntity.ok(empEntity);
	}
	
	@GetMapping("readAll")
	public ResponseEntity<List<EmployeeEntity>> readEmployees () {
		
		List<EmployeeEntity> employess = employeeService.readEmployees();
		 return ResponseEntity.ok(employess);
	}
	
	@GetMapping("readOne/{id}")
	public ResponseEntity<?> readEmployee (@PathVariable("id") Integer empID) throws Exception {
		
		EmployeeEntity employee = employeeService.readOneEmployees(empID);
		 return ResponseEntity.ok(employee);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteEmployee (@PathVariable("id") Integer empID) throws Exception {		
		HttpStatus status =  employeeService.deleteEmployee(empID);
		return ResponseEntity.ok(status);
	}
	
	@GetMapping("/getDepartmentsNames")
	public ResponseEntity<?> getDepartmentsNames () {	
		 List<DepartmentEntity> departments = employeeService.getAllDepartmentsByName();
		 return ResponseEntity.ok(departments);
	}
	
}
