package com.etisalat.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.etisalat.entities.DepartmentEntity;
import com.etisalat.service.DepartmentService;

@Controller
@CrossOrigin(origins = {"http://localhost:8081","http://localhost:3000"})
@RequestMapping("/department")
public class DepartmentContoller {
	
	@Autowired
	DepartmentService departmentService;
	
	
	@PostMapping("/createOrUpdate")
	public ResponseEntity<?> createDepartment (@RequestBody DepartmentEntity Department) {
		
		DepartmentEntity empEntity = departmentService.createOrUpdateDepartment(Department);
		
		return  ResponseEntity.ok(empEntity);
	}
	
	@GetMapping("readAll")
	public ResponseEntity<?> readDepartments () {
		
		List<DepartmentEntity> employess = departmentService.readDepartments();
		 return ResponseEntity.ok(employess);
	}
	
	@GetMapping("readOne/{id}")
	public ResponseEntity<?> readDepartment (@PathVariable("id") Integer depID) throws Exception {
		
		DepartmentEntity Department = departmentService.readOneDepartments(depID);
		 return ResponseEntity.ok(Department);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteDepartment (@PathVariable("id") Integer depID) throws Exception {		
		HttpStatus status =  departmentService.deleteDepartment(depID);
		return ResponseEntity.ok(status);
	}

}
